QT += quick widgets concurrent quickcontrols2 network
CONFIG += c++1z

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    addon.cpp \
    control.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    addon.h \
    control.h \
    utils.h

LIBS += -lgit2

win32 {
    DEFINES += _WIN32_WINNT=0x06010000 GAM_EXEC=\\\"$${TARGET}.exe\\\"
}
unix {
    DEFINES += GAM_EXEC=\\\"$${TARGET}\\\"
}

isEmpty(GIT_DESCRIBE){
    GIT_DESCRIBE = $$system(git describe --tags --long)
}
isEmpty(GIT_DESCRIBE){
    message("WARNING: empty git describe")
} else {
    message("Git describe: $$GIT_DESCRIBE")
}

DEFINES += GIT_DESCRIBE=\\\"$$GIT_DESCRIBE\\\"

DISTFILES += \
    README.md

QMAKE_CXXFLAGS += -fconcepts
