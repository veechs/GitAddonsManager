# GitAddonsManager

Git based World of Warcraft addon manager.

This application exclusively manage git repositories, addons not containing a git repositories will be ignored. All addons hosted on Gitlab, Github and likes are compatible but must be cloned through git (if you don't know what that means, you will need to redownload your addons through this app).
